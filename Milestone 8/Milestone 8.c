#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
 
 
int int_cmp(const void *a, const void *b) 
{ 
    const int *ia = (const int *)a; 
    const int *ib = (const int *)b;
    return *ia  - *ib; 
	 
} 
 


 
 
void sortint() 
{ 
    int numbers[] = { 23,22,-100,27,4,6,7,-1,-2,78,0,1 }; 
    int len = sizeof(numbers)/sizeof(int);
 
    printf("----Integer sorting----");
 
    printf("\n");
    int i;
 
    for(i=0; i<len; i++) 
        printf("%d,", numbers[i]);
 
printf("\n");
 
  
    qsort(numbers, len, sizeof(int), int_cmp);
 
   
    
 
    for(i=0; i<len; i++) 
        printf("%d,", numbers[i]);
 
  printf("\n");
} 
 

int cstring_cmp(const void *a, const void *b) 
{ 
    const char **ia = (const char **)a;
    const char **ib = (const char **)b;
    return strcmp(*ia, *ib);
	
} 
 

 

void sortstr() 
{ 
    char *strings[] = {"anup","ashu","shreya","kafil","jitty" };
    int len = sizeof(strings) / sizeof(char*);
 
   
    printf("----String sorting----");
	printf("\n");
    
    int i;
 
    for(i=0; i<len; i++) 
        printf("%s,", strings[i]);
 
    printf("\n");
 
    qsort(strings, len, sizeof(char *), cstring_cmp);
 
   
    
 
    for(i=0; i<len; i++) 
        printf("%s,", strings[i]);
 
    printf("\n");
} 
 
 int main() 
{ 
    
    sortint();
    sortstr();
   
    return 0;
} 
 