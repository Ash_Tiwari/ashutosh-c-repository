#include<conio.h>
#include<stdio.h>

int fact(int z);

void main()
{
	int n,r,ncr;
	printf("Enter val of n and r ");
	scanf("%d%d",&n,&r);
	ncr=fact(n)/(fact(r)*fact(n-r));
	printf("ans=%d",ncr);
	getch();
}

int fact(int z)
{
	int i, f=1;
	if(z==0)
	{
		return(f);
	}
	else
	{
		for(i=1;i<=z;i++)
		{
			f=f*i;
		}
	}
	return(f);
}