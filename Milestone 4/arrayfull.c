#include<stdio.h>
#include<stdlib.h>

int array[8];
int n=0,i,j,temp;

void insert();
void delete();
void search();
void sort();
void print_arr();
void asc();
void dsc();

void main()
{
        int choice;
        do{
                
		printf("1.Add\n");
                printf("2.Search\n");
                printf("3.Remove\n");
                printf("4.Sort\n");
                printf("5.Display\n");
                printf("6.Exit\n");
                
                
                printf("\nEnter Choice:\t");
                scanf("%d",&choice);
                switch(choice)
                {
                        case 1: insert();
                                break;

                        case 2: search();
                                break;

                        case 3: delete();
                                break; 
                       
                        case 4: sort();
                                break;

                        case 5: print_arr();
                                break;
                        
                        case 6: exit(0);
                                break;

                        default: printf("\nEnter Valid Choice:\n");
                                 break;
                }
        }while(choice!=6);

}


void insert()
{
	if(n<=7)
	{
		int flag=0,ele,j,i;
		printf("Enter the element:-");
		scanf("%d",&ele);

		for(i=0;i<=n;i++)
		{
			if(array[i]==ele)
			{
				flag=1;
			}
		}

		if(flag==1)
		{
			printf("Element cant be repeated.\n");
		}
		else
		{
			array[n]=ele;
			printf("Element added.\n");
			n=n+1;
		}
	}
 	
	else
	{
		printf("The array is full\n");
	}
}


void print_arr()
{
	int i=0;
	printf("The elements are:");

	for(i=0;i<n;i++)
	{
		printf("%d,",array[i]);
	}

	printf("\n");
}


void search()
{
	int flag=0,target_ele,temp,i=0;
	printf("Enter the element to be search");
	scanf("%d",&target_ele);
	for(i=0;i<n;i++)
	{
		if(array[i]==target_ele)
		{
			flag=1;
			temp=i;
			break;
		}
	}

	if(flag==1)
	{
		printf("Found %d at index:%d\n",target_ele,temp);
	}

	else
	{
		printf("%d is not present\n",target_ele);
	}
}


void sort()
{
	int pref;
	printf("1.Ascending\n");
	printf("2.Descending\n");
	scanf("%d",&pref);
	if(pref==1)
	{
		asc();
	}
	else if(pref==2)
	{
		dsc();
	}
}


void asc()
{
	int temp,j=0,i=0,k=0;
	for(i=0;i<n;i++)
	{
		for(j=0;j<n-1;j++)
		{
			if(array[j]>array[j+1])
			{
				temp=array[j];
				array[j]=array[j+1];
				array[j+1]=temp;
			}
		}
	}

	printf("\nThe array after sorting is:");

	for(k=0;k<n;k++)
	{
		printf("%d,",array[k]);
	}

	printf("\n");
}


void dsc()
{
	int temp,j=0,i=0,k=0;
	for(i=0;i<n;i++)
	{
		for(j=0;j<n-1;j++)
		{
			if(array[j]<array[j+1])
			{
				temp=array[j];
				array[j]=array[j+1];
				array[j+1]=temp;
			}
		}
	}

	printf("\nThe array after sorting is:");

	for(k=0;k<n;k++)
	{
		printf("%d,",array[k]);
	}

	printf("\n");
}

void delete()
{
	int flag=0,target_ele1,i=0;
	printf("Enter the element to be deleted");
	scanf("%d",&target_ele1);
	for(i=0;i<n;i++)
	{
		if(array[i]==target_ele1)
		{
			flag=1;
			temp=i;
			break;
		}
	}
	if(flag==1)
	{
		for(j=temp;j<n-1;j++)
		{
			array[j]=array[j+1];
		}
		printf("Element removed\n");
		n=n-1;	
	}
	else if(flag==0)
	{
		printf("%d is not removed\n",target_ele1);
	}
	
}
